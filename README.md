# Jot

Jot is a little notebook. It makes little notepages with timestamps that you can customize. By default it uses vim and
puts stuff in notes/year/month/day.md. Try it with

    stack install jot
    jot

You can customize the configuration in configs/config.dhall and run.

    jot --config=path/to/config.dhall

I might make this more better later. You can use either dhall or yaml for the config.

A good way to use this tool would be to clone the
[GitbookTemplate](http://gitab.com/locallycompact/GitbookTemplate) repository
(example rendering [here](http://locallycompact.gitlab.io/GitbookTemplate/)) and then
sit in that and go 'jot' when you wanna write something.
