{ dayFormat     = "%A %d %B"
, editor        = "vim"
, editorOptions = ["+"]
, logBook       = "notes"
, timeFormat    = "%H:%M" }
